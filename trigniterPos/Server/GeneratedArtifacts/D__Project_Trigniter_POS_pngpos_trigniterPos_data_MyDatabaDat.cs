﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

[assembly: EdmSchemaAttribute()]
namespace D__Project_Trigniter_POS_pngpos_trigniterPos_data_MyDatabaDat.Implementation
{
    #region Contexts
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    public partial class D__Project_Trigniter_POS_pngpos_trigniterPos_data_MyDatabaDatObjectContext : ObjectContext
    {
        #region Constructors
    
        /// <summary>
        /// Initializes a new D__Project_Trigniter_POS_pngpos_trigniterPos_data_MyDatabaDatObjectContext object using the connection string found in the 'D__Project_Trigniter_POS_pngpos_trigniterPos_data_MyDatabaDatObjectContext' section of the application configuration file.
        /// </summary>
        public D__Project_Trigniter_POS_pngpos_trigniterPos_data_MyDatabaDatObjectContext() : base("name=D__Project_Trigniter_POS_pngpos_trigniterPos_data_MyDatabaDatObjectContext", "D__Project_Trigniter_POS_pngpos_trigniterPos_data_MyDatabaDatObjectContext")
        {
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new D__Project_Trigniter_POS_pngpos_trigniterPos_data_MyDatabaDatObjectContext object.
        /// </summary>
        public D__Project_Trigniter_POS_pngpos_trigniterPos_data_MyDatabaDatObjectContext(string connectionString) : base(connectionString, "D__Project_Trigniter_POS_pngpos_trigniterPos_data_MyDatabaDatObjectContext")
        {
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new D__Project_Trigniter_POS_pngpos_trigniterPos_data_MyDatabaDatObjectContext object.
        /// </summary>
        public D__Project_Trigniter_POS_pngpos_trigniterPos_data_MyDatabaDatObjectContext(EntityConnection connection) : base(connection, "D__Project_Trigniter_POS_pngpos_trigniterPos_data_MyDatabaDatObjectContext")
        {
            OnContextCreated();
        }
    
        #endregion
    
        #region Partial Methods
    
        partial void OnContextCreated();
    
        #endregion
    
    }

    #endregion

    
}
