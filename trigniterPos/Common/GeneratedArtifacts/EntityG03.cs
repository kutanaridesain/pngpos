﻿

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LightSwitchApplication
{
    #region Entities
    
    /// <summary>
    /// No Modeled Description Available
    /// </summary>
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.LightSwitch.BuildTasks.CodeGen", "11.0.0.0")]
    public sealed partial class CustomerProductDiscount : global::Microsoft.LightSwitch.Framework.Base.EntityObject<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass>
    {
        #region Constructors
    
        /// <summary>
        /// Initializes a new instance of the CustomerProductDiscount entity.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.LightSwitch.BuildTasks.CodeGen", "11.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public CustomerProductDiscount()
            : this(null)
        {
        }
    
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.LightSwitch.BuildTasks.CodeGen", "11.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public CustomerProductDiscount(global::Microsoft.LightSwitch.Framework.EntitySet<global::LightSwitchApplication.CustomerProductDiscount> entitySet)
            : base(entitySet)
        {
            global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.Initialize(this);
        }
    
        [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        partial void CustomerProductDiscount_Created();
        [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        partial void CustomerProductDiscount_AllowSaveWithErrors(ref bool result);
    
        #endregion
    
        #region Private Properties
        
        /// <summary>
        /// Gets the Application object for this application.  The Application object provides access to active screens, methods to open screens and access to the current user.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.LightSwitch.BuildTasks.CodeGen", "11.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private global::Microsoft.LightSwitch.IApplication<global::LightSwitchApplication.DataWorkspace> Application
        {
            get
            {
                return global::LightSwitchApplication.Application.Current;
            }
        }
        
        /// <summary>
        /// Gets the containing data workspace.  The data workspace provides access to all data sources in the application.
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.LightSwitch.BuildTasks.CodeGen", "11.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private global::LightSwitchApplication.DataWorkspace DataWorkspace
        {
            get
            {
                return (global::LightSwitchApplication.DataWorkspace)this.Details.EntitySet.Details.DataService.Details.DataWorkspace;
            }
        }
        
        #endregion
    
        #region Public Properties
    
        /// <summary>
        /// No Modeled Description Available
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.LightSwitch.BuildTasks.CodeGen", "11.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public long Id
        {
            get
            {
                return global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.GetValue(this, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Id);
            }
        }
        
        [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        partial void Id_IsReadOnly(ref bool result);
        [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        partial void Id_Validate(global::Microsoft.LightSwitch.EntityValidationResultsBuilder results);
        [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        partial void Id_Changed();

        /// <summary>
        /// No Modeled Description Available
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.LightSwitch.BuildTasks.CodeGen", "11.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::LightSwitchApplication.Customer Customer
        {
            get
            {
                return global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.GetValue(this, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Customer);
            }
            set
            {
                global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.SetValue(this, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Customer, value);
            }
        }
        
        [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        partial void Customer_IsReadOnly(ref bool result);
        [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        partial void Customer_Validate(global::Microsoft.LightSwitch.EntityValidationResultsBuilder results);
        [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        partial void Customer_Changed();

        /// <summary>
        /// No Modeled Description Available
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.LightSwitch.BuildTasks.CodeGen", "11.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::LightSwitchApplication.Discount Discount
        {
            get
            {
                return global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.GetValue(this, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Discount);
            }
            set
            {
                global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.SetValue(this, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Discount, value);
            }
        }
        
        [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        partial void Discount_IsReadOnly(ref bool result);
        [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        partial void Discount_Validate(global::Microsoft.LightSwitch.EntityValidationResultsBuilder results);
        [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        partial void Discount_Changed();

        #endregion
    
        #region Details Class
    
        [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Never)]
        [global::System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.LightSwitch.BuildTasks.CodeGen", "11.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public sealed class DetailsClass : global::Microsoft.LightSwitch.Details.Framework.Base.EntityDetails<
                global::LightSwitchApplication.CustomerProductDiscount,
                global::LightSwitchApplication.CustomerProductDiscount.DetailsClass,
                global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.IImplementation,
                global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySet,
                global::Microsoft.LightSwitch.Details.Framework.EntityCommandSet<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass>,
                global::Microsoft.LightSwitch.Details.Framework.EntityMethodSet<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass>>
        {
    
            static DetailsClass()
            {
                var initializeEntry = global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Id;
            }
    
            [global::System.Diagnostics.DebuggerBrowsable(global::System.Diagnostics.DebuggerBrowsableState.Never)]
            private static readonly global::Microsoft.LightSwitch.Details.Framework.Base.EntityDetails<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass>.Entry
                __CustomerProductDiscountEntry = new global::Microsoft.LightSwitch.Details.Framework.Base.EntityDetails<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass>.Entry(
                    global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.__CustomerProductDiscount_CreateNew,
                    global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.__CustomerProductDiscount_Created,
                    global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.__CustomerProductDiscount_AllowSaveWithErrors);
            private static global::LightSwitchApplication.CustomerProductDiscount __CustomerProductDiscount_CreateNew(global::Microsoft.LightSwitch.Framework.EntitySet<global::LightSwitchApplication.CustomerProductDiscount> es)
            {
                return new global::LightSwitchApplication.CustomerProductDiscount(es);
            }
            private static void __CustomerProductDiscount_Created(global::LightSwitchApplication.CustomerProductDiscount e)
            {
                e.CustomerProductDiscount_Created();
            }
            private static bool __CustomerProductDiscount_AllowSaveWithErrors(global::LightSwitchApplication.CustomerProductDiscount e)
            {
                bool result = false;
                e.CustomerProductDiscount_AllowSaveWithErrors(ref result);
                return result;
            }
    
            public DetailsClass() : base()
            {
            }
    
            public new global::Microsoft.LightSwitch.Details.Framework.EntityCommandSet<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass> Commands
            {
                get
                {
                    return base.Commands;
                }
            }
    
            public new global::Microsoft.LightSwitch.Details.Framework.EntityMethodSet<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass> Methods
            {
                get
                {
                    return base.Methods;
                }
            }
    
            public new global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySet Properties
            {
                get
                {
                    return base.Properties;
                }
            }
    
            [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Never)]
            [global::System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
            [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.LightSwitch.BuildTasks.CodeGen", "11.0.0.0")]
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public sealed class PropertySet : global::Microsoft.LightSwitch.Details.Framework.Base.EntityPropertySet<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass>
            {
    
                public PropertySet() : base()
                {
                }
    
                public global::Microsoft.LightSwitch.Details.Framework.EntityStorageProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, long> Id
                {
                    get
                    {
                        return base.GetItem(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Id) as global::Microsoft.LightSwitch.Details.Framework.EntityStorageProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, long>;
                    }
                }
                
                public global::Microsoft.LightSwitch.Details.Framework.EntityReferenceProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::LightSwitchApplication.Customer> Customer
                {
                    get
                    {
                        return base.GetItem(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Customer) as global::Microsoft.LightSwitch.Details.Framework.EntityReferenceProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::LightSwitchApplication.Customer>;
                    }
                }
                
                public global::Microsoft.LightSwitch.Details.Framework.EntityReferenceProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::LightSwitchApplication.Discount> Discount
                {
                    get
                    {
                        return base.GetItem(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Discount) as global::Microsoft.LightSwitch.Details.Framework.EntityReferenceProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::LightSwitchApplication.Discount>;
                    }
                }
                
            }
    
            #pragma warning disable 109
            [global::System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")]
            public interface IImplementation : global::Microsoft.LightSwitch.Internal.IEntityImplementation
            {
                new long Id { get; }
                new global::Microsoft.LightSwitch.Internal.IEntityImplementation Customer { get; set; }
                new global::Microsoft.LightSwitch.Internal.IEntityImplementation Discount { get; set; }
            }
            #pragma warning restore 109
    
            [global::System.ComponentModel.EditorBrowsable(global::System.ComponentModel.EditorBrowsableState.Never)]
            [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.LightSwitch.BuildTasks.CodeGen", "11.0.0.0")]
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            internal class PropertySetProperties
            {
    
                [global::System.Diagnostics.DebuggerBrowsable(global::System.Diagnostics.DebuggerBrowsableState.Never)]
                public static readonly global::Microsoft.LightSwitch.Details.Framework.EntityStorageProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, long>.Entry
                    Id = new global::Microsoft.LightSwitch.Details.Framework.EntityStorageProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, long>.Entry(
                        "Id",
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Id_Stub,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Id_ComputeIsReadOnly,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Id_Validate,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Id_GetImplementationValue,
                        null,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Id_OnValueChanged);
                private static void _Id_Stub(global::Microsoft.LightSwitch.Details.Framework.Base.DetailsCallback<global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::Microsoft.LightSwitch.Details.Framework.EntityStorageProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, long>.Data> c, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass d, object sf)
                {
                    c(d, ref d._Id, sf);
                }
                private static bool _Id_ComputeIsReadOnly(global::LightSwitchApplication.CustomerProductDiscount e)
                {
                    bool result = false;
                    e.Id_IsReadOnly(ref result);
                    return result;
                }
                private static void _Id_Validate(global::LightSwitchApplication.CustomerProductDiscount e, global::Microsoft.LightSwitch.EntityValidationResultsBuilder r)
                {
                    e.Id_Validate(r);
                }
                private static long _Id_GetImplementationValue(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass d)
                {
                    return d.ImplementationEntity.Id;
                }
                private static void _Id_OnValueChanged(global::LightSwitchApplication.CustomerProductDiscount e)
                {
                    e.Id_Changed();
                }
    
                [global::System.Diagnostics.DebuggerBrowsable(global::System.Diagnostics.DebuggerBrowsableState.Never)]
                public static readonly global::Microsoft.LightSwitch.Details.Framework.EntityReferenceProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::LightSwitchApplication.Customer>.Entry
                    Customer = new global::Microsoft.LightSwitch.Details.Framework.EntityReferenceProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::LightSwitchApplication.Customer>.Entry(
                        "Customer",
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Customer_Stub,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Customer_ComputeIsReadOnly,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Customer_Validate,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Customer_GetCoreImplementationValue,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Customer_GetImplementationValue,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Customer_SetImplementationValue,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Customer_Refresh,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Customer_OnValueChanged);
                private static void _Customer_Stub(global::Microsoft.LightSwitch.Details.Framework.Base.DetailsCallback<global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::Microsoft.LightSwitch.Details.Framework.EntityReferenceProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::LightSwitchApplication.Customer>.Data> c, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass d, object sf)
                {
                    c(d, ref d._Customer, sf);
                }
                private static bool _Customer_ComputeIsReadOnly(global::LightSwitchApplication.CustomerProductDiscount e)
                {
                    bool result = false;
                    e.Customer_IsReadOnly(ref result);
                    return result;
                }
                private static void _Customer_Validate(global::LightSwitchApplication.CustomerProductDiscount e, global::Microsoft.LightSwitch.EntityValidationResultsBuilder r)
                {
                    e.Customer_Validate(r);
                }
                private static global::Microsoft.LightSwitch.Internal.IEntityImplementation _Customer_GetCoreImplementationValue(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass d)
                {
                    return d.ImplementationEntity.Customer;
                }
                private static global::LightSwitchApplication.Customer _Customer_GetImplementationValue(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass d)
                {
                    return d.GetImplementationValue<global::LightSwitchApplication.Customer, global::LightSwitchApplication.Customer.DetailsClass>(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Customer, ref d._Customer);
                }
                private static void _Customer_SetImplementationValue(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass d, global::LightSwitchApplication.Customer v)
                {
                    d.SetImplementationValue(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Customer, ref d._Customer, (i, ev) => i.Customer = ev, v);
                }
                private static void _Customer_Refresh(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass d)
                {
                    d.RefreshNavigationProperty(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Customer, ref d._Customer);
                }
                private static void _Customer_OnValueChanged(global::LightSwitchApplication.CustomerProductDiscount e)
                {
                    e.Customer_Changed();
                }
    
                [global::System.Diagnostics.DebuggerBrowsable(global::System.Diagnostics.DebuggerBrowsableState.Never)]
                public static readonly global::Microsoft.LightSwitch.Details.Framework.EntityReferenceProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::LightSwitchApplication.Discount>.Entry
                    Discount = new global::Microsoft.LightSwitch.Details.Framework.EntityReferenceProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::LightSwitchApplication.Discount>.Entry(
                        "Discount",
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Discount_Stub,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Discount_ComputeIsReadOnly,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Discount_Validate,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Discount_GetCoreImplementationValue,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Discount_GetImplementationValue,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Discount_SetImplementationValue,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Discount_Refresh,
                        global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties._Discount_OnValueChanged);
                private static void _Discount_Stub(global::Microsoft.LightSwitch.Details.Framework.Base.DetailsCallback<global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::Microsoft.LightSwitch.Details.Framework.EntityReferenceProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::LightSwitchApplication.Discount>.Data> c, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass d, object sf)
                {
                    c(d, ref d._Discount, sf);
                }
                private static bool _Discount_ComputeIsReadOnly(global::LightSwitchApplication.CustomerProductDiscount e)
                {
                    bool result = false;
                    e.Discount_IsReadOnly(ref result);
                    return result;
                }
                private static void _Discount_Validate(global::LightSwitchApplication.CustomerProductDiscount e, global::Microsoft.LightSwitch.EntityValidationResultsBuilder r)
                {
                    e.Discount_Validate(r);
                }
                private static global::Microsoft.LightSwitch.Internal.IEntityImplementation _Discount_GetCoreImplementationValue(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass d)
                {
                    return d.ImplementationEntity.Discount;
                }
                private static global::LightSwitchApplication.Discount _Discount_GetImplementationValue(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass d)
                {
                    return d.GetImplementationValue<global::LightSwitchApplication.Discount, global::LightSwitchApplication.Discount.DetailsClass>(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Discount, ref d._Discount);
                }
                private static void _Discount_SetImplementationValue(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass d, global::LightSwitchApplication.Discount v)
                {
                    d.SetImplementationValue(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Discount, ref d._Discount, (i, ev) => i.Discount = ev, v);
                }
                private static void _Discount_Refresh(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass d)
                {
                    d.RefreshNavigationProperty(global::LightSwitchApplication.CustomerProductDiscount.DetailsClass.PropertySetProperties.Discount, ref d._Discount);
                }
                private static void _Discount_OnValueChanged(global::LightSwitchApplication.CustomerProductDiscount e)
                {
                    e.Discount_Changed();
                }
    
            }
    
            [global::System.Diagnostics.DebuggerBrowsable(global::System.Diagnostics.DebuggerBrowsableState.Never)]
            private global::Microsoft.LightSwitch.Details.Framework.EntityStorageProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, long>.Data _Id;
            
            [global::System.Diagnostics.DebuggerBrowsable(global::System.Diagnostics.DebuggerBrowsableState.Never)]
            private global::Microsoft.LightSwitch.Details.Framework.EntityReferenceProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::LightSwitchApplication.Customer>.Data _Customer;
            
            [global::System.Diagnostics.DebuggerBrowsable(global::System.Diagnostics.DebuggerBrowsableState.Never)]
            private global::Microsoft.LightSwitch.Details.Framework.EntityReferenceProperty<global::LightSwitchApplication.CustomerProductDiscount, global::LightSwitchApplication.CustomerProductDiscount.DetailsClass, global::LightSwitchApplication.Discount>.Data _Discount;
            
        }
    
        #endregion
    }
    
    #endregion
}
