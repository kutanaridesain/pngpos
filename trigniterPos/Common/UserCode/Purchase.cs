﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.LightSwitch;
namespace LightSwitchApplication
{
    public partial class Purchase
    {
        partial void Total_Compute(ref decimal result)
        {
            // Set result to the desired field value
            result = this.PurchaseDetails.Sum(p => p.Price) * this.PurchaseDetails.Sum(p => p.Quantity);
        }
    }
}
