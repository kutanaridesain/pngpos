﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.LightSwitch;
namespace LightSwitchApplication
{
    public partial class SaleDetail
    {
        partial void DiscountComputed_Compute(ref double result)
        {
            // Set result to the desired field value
            this.Discount = this.Sale.Customer.CustomerProductDiscounts.Sum(x => x.Discount.Value);
             result = (Double)this.Discount;
        }
    }
}
